<?php
// src/AppBundle/Form/ProfileFormType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('about')
               ;

    //             ,TextType::class,array( 'attr' => array(
    // 'class'=>'',
    // 'id'=>'', 
    // 'name'=>'', 
    // 'step'=>'',
    // 'placeholder' => ''))

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profedit';
    }


}
