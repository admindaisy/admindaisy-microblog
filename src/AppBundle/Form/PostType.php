<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username',HiddenType::class)
                
                ->add('title',TextType::class,array('attr' => array(
                'class'=>'txbox',
                'id'=>'title', 
                'name'=>'title', 
                'placeholder' => 'Title')))
                
                ->add('topic',TextType::class,array('attr' => array(
                'class'=>'txbox',
                'id'=>'topic', 
                'name'=>'topic', 
                'list'=>'browsers',
                'placeholder' => 'Topic')))
                
                ->add('blog_mess',TextareaType::class,array(    'attr' => array(
                'class'=>'txarea',
                'id'=>'blog', 
                'name'=>'blog', 
                'rows'=>'8',
                'cols'=>'50',   
                'placeholder' => 'Write a blog ...')))

                ->add('blog_img',FileType::class,array( 'attr' => array(
                'id'=>'attachedfile', 
                'name'=>'attachedfile', 
                'maxlength'=>'50',
                'allow' => 'text/*')))
                
                ->add('posted_at',HiddenType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_post';
    }


}
