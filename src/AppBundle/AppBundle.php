<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{

	public function getParent()//extend FOSuser bundle for use of controller.
    {
        return 'FOSUserBundle';
    }
}
