<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_ea5d1435ea6276b230ec6a396de8ae8dd39da66a0c42d1026f7e3d4e1383ccce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 3
        echo "
";
        // line 4
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 5
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 7
        echo "
    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"> 
        </div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-login card-plain\">
                        <div class=\"header header-primary text-center\">
                            <div class=\"logo-container\">
                                <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/blog.png"), "html", null, true);
        echo "\" alt=\"\">
                            </div>
                        </div>
                        <form action=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                        ";
        // line 20
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : null)) {
            // line 21
            echo "                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
            echo "\" />
                        ";
        }
        // line 23
        echo "                        <div class=\"content\">
                            ";
        // line 25
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                                ";
        // line 28
        echo "                                <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" placeholder=\"Username\" />
                            </div>
                            ";
        // line 31
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                                ";
        // line 34
        echo "                                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"Password\" />
                            </div>
                        </div>
                        <div class=\"footer text-center\">
                            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-round btn-lg btn-block\" />
                        </div>
                        </form>
                        <div class=\"pull-right\">
                            <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\" class=\"link\"><i class=\"fa fa-caret-right\"></i> SIGN UP</a>
                        </div>
                    
                </div>
            </div>
        </div>
    ";
        // line 48
        $this->loadTemplate("body/body_footer.html.twig", "FOSUserBundle:Security:login_content.html.twig", 48)->display($context);
        // line 49
        echo "
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 49,  104 => 48,  95 => 42,  88 => 38,  82 => 34,  78 => 31,  72 => 28,  68 => 25,  65 => 23,  59 => 21,  57 => 20,  53 => 19,  47 => 16,  37 => 9,  33 => 7,  27 => 5,  25 => 4,  22 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login_content.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
