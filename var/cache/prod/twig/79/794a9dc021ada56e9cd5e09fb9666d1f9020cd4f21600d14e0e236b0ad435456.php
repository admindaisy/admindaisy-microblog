<?php

/* :body:body_footer.html.twig */
class __TwigTemplate_5d454c6d2f00e3652fd69b8ac2c20f8138893d0c46d836c71e22f3d5ca0cfa3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body_footer' => array($this, 'block_body_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('body_footer', $context, $blocks);
    }

    public function block_body_footer($context, array $blocks = array())
    {
        // line 2
        echo "      ";
        // line 3
        echo "            <footer class=\"footer\">
                <div class=\"container\">
                    <div class=\"copyright\">&copy;
                        <script>document.write(new Date().getFullYear())</script>, Submitted by
                        <a href=\"#\" target=\"_blank\">LATEST GROUP</a>. Required project for  
                        <a href=\"#\" target=\"_blank\">BSIT ELECTIVE 3</a>.
                    </div>
                </div>
            </footer>    
        </div>
";
    }

    public function getTemplateName()
    {
        return ":body:body_footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  26 => 2,  20 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":body:body_footer.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_footer.html.twig");
    }
}
