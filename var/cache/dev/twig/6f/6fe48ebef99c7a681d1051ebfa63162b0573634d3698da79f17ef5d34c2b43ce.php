<?php

/* body/body_profile.html.twig */
class __TwigTemplate_ff6de80ee07795a493c8891e3da894e935d3ad62b278bbb719e62aff39aaf5be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "body/body_profile.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a69e4f5b110d7db77b43c565358c5f71620aed75d51d65c3522c56e3bc8eb6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a69e4f5b110d7db77b43c565358c5f71620aed75d51d65c3522c56e3bc8eb6f->enter($__internal_8a69e4f5b110d7db77b43c565358c5f71620aed75d51d65c3522c56e3bc8eb6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_profile.html.twig"));

        $__internal_6b876b98f7695e97d609c632cb6865a39b57220e4d7fcf288e6b96c7c39fbb15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b876b98f7695e97d609c632cb6865a39b57220e4d7fcf288e6b96c7c39fbb15->enter($__internal_6b876b98f7695e97d609c632cb6865a39b57220e4d7fcf288e6b96c7c39fbb15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_profile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a69e4f5b110d7db77b43c565358c5f71620aed75d51d65c3522c56e3bc8eb6f->leave($__internal_8a69e4f5b110d7db77b43c565358c5f71620aed75d51d65c3522c56e3bc8eb6f_prof);

        
        $__internal_6b876b98f7695e97d609c632cb6865a39b57220e4d7fcf288e6b96c7c39fbb15->leave($__internal_6b876b98f7695e97d609c632cb6865a39b57220e4d7fcf288e6b96c7c39fbb15_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_19d9fdadedac87e313f71808c3771405fc471cd7f814b4508e10234f0d20cd4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19d9fdadedac87e313f71808c3771405fc471cd7f814b4508e10234f0d20cd4d->enter($__internal_19d9fdadedac87e313f71808c3771405fc471cd7f814b4508e10234f0d20cd4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f82dcc07e7c8532a7d71c304ab8c4fedb3567f0c5d4ccb2ce9df15c9bb2107a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f82dcc07e7c8532a7d71c304ab8c4fedb3567f0c5d4ccb2ce9df15c9bb2107a->enter($__internal_6f82dcc07e7c8532a7d71c304ab8c4fedb3567f0c5d4ccb2ce9df15c9bb2107a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    
        <div class=\"page-header page-header-small\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image: url(";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets//img/bgg6.jpg"), "html", null, true);
        echo ");\">
            </div>
            <div class=\"container\">
                <div class=\"content-center\">
                    <div class=\"photo-container\" style=\"padding-top: 170px;\">
                        <img src=\"\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 170px; width: 170px;\">
                        <h4 class=\"title\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "name", array()), "html", null, true);
        echo "</h4>
                        <p class=\"category\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class=\"section\">
            <div class=\"container\">
                ";
        // line 21
        echo "                <h3 class=\"title\">About me</h3>
                <h5 class=\"description\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "about", array()), "html", null, true);
        echo "</h5>
                ";
        // line 24
        echo "                
                ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title text-primary\"><i class=\"fa fa-edit\"></i> Blog</h3>
                    </div>
                    ";
        // line 31
        echo "                    <div class=\"col-md-12\">
                      
                        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "title", array()), 'widget');
        echo "

                    </div>
                    ";
        // line 37
        echo "                    <div class=\"col-md-12\">
                        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "topic", array()), 'widget');
        echo "                        
                        <datalist id=\"browsers\">  
                            <option value=\"Self improvement/Self-Hypnosis\">
                            <option value=\"Health & Fitness for Busy People\">
                            <option value=\"Language Learning Blogs\">
                            <option value=\"How to Travel on a Budget (Best hotel deals. Car rental. Trip advice.)\">
                            <option value=\"Writing Style\">
                            <option value=\"Rescued Animals\">
                            <option value=\"Ghost-hunting\">
                            <option value=\"Healthy eating blog\">
                            <option value=\"Beyond the basics of personal financial management\">
                            <option value=\"Hamburgers\">
                            <option value=\"Getting Microsoft Certified\">
                            <option value=\"Careers vs job: following your passion\">
                            <option value=\"How to have a Strong Marriage\">
                            <option value=\"Getting Microsoft Certified\">
                        </datalist>
                    </div>
                    ";
        // line 57
        echo "                    <div class=\"col-md-12\">
                        ";
        // line 58
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "blog_mess", array()), 'widget');
        echo "                        
                    </div>
                    ";
        // line 61
        echo "                   <i class=\"fa fa-paperclip\"></i> Attachment: 
                        ";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "blog_img", array()), 'widget');
        echo "                        
                    <div class=\"col-sm-6\"></div>
                        <a href=\"#\" class=\"btn btn-default Default\">CLEAR</a>
                        <input type=\"submit\" class=\"btn btn-primary Primary\" value=\"POST\" />
                </div>   
                ";
        // line 67
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    

    
        
   

    ";
        // line 80
        echo "

                ";
        // line 83
        echo "

                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title\" style=\"margin-top:20px;\">Posted Blog</h3>
                    </div>
                    ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            echo " 
                    <div class=\"row\">       
                        ";
            // line 92
            echo "                        
                        ";
            // line 106
            echo "                        ";
            echo "   
                        <div class=\"col-md-12\"></div>
                        <div class=\"col-md-3 text-center\">
                            <img src=\"\" alt=\"Thumbnail Image\" class=\"rounded-circle img-raised2\" style=\"height: 120px; width: 120px; margin-top:20px;\"><br>
                            <h7 style=\"color: gray;\">";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "username", array()), "html", null, true);
            echo "<br>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "posted_at", array()), "html", null, true);
            echo "</h7>
                        </div>
                        
                            <div class=\"col-md-9\" style=\"padding-left: 0px;\">
                                ";
            // line 115
            echo "                                <h6 class=\"title\" style=\"margin-top:20px;\">TITLE: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "title", array()), "html", null, true);
            echo "</h6>
                                <p>TOPIC: ";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "topic", array()), "html", null, true);
            echo "</p>
                                <textarea disabled class=\"distxarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\">";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "blog_mess", array()), "html", null, true);
            echo "</textarea>
                            </div>
                    </div> 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                </div>
            </div>
        </div>

        <div class=\"modal fade modal-mini modal-primary\" id=\"modal_edit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                    <div class=\"modal-header justify-content-center\">
                        <div class=\"modal-profile\">
                            <i class=\"fa fa-edit\"></i>
                        </div>
                    </div>
                    <div class=\"modal-body\">
                        <div class=\"col-md-12\">
                            <textarea name=\"editblog\" class=\"txarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\"></textarea>
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        
                        <button type=\"button\" class=\"btn btn-link btn-neutral\">Save</button>
                        <button type=\"button\" class=\"btn btn-link btn-neutral\" data-dismiss=\"modal\">Discard</button>
                    </div>
                </div>
            </div>
        </div>

";
        
        $__internal_6f82dcc07e7c8532a7d71c304ab8c4fedb3567f0c5d4ccb2ce9df15c9bb2107a->leave($__internal_6f82dcc07e7c8532a7d71c304ab8c4fedb3567f0c5d4ccb2ce9df15c9bb2107a_prof);

        
        $__internal_19d9fdadedac87e313f71808c3771405fc471cd7f814b4508e10234f0d20cd4d->leave($__internal_19d9fdadedac87e313f71808c3771405fc471cd7f814b4508e10234f0d20cd4d_prof);

    }

    public function getTemplateName()
    {
        return "body/body_profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 121,  205 => 117,  201 => 116,  196 => 115,  187 => 110,  180 => 106,  177 => 92,  170 => 89,  162 => 83,  158 => 80,  147 => 67,  139 => 62,  136 => 61,  131 => 58,  128 => 57,  107 => 38,  104 => 37,  98 => 33,  94 => 31,  86 => 25,  83 => 24,  79 => 22,  76 => 21,  66 => 13,  62 => 12,  53 => 6,  49 => 4,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body%}
{# Body #}
    
        <div class=\"page-header page-header-small\" filter-color=\"orange\">
            <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image: url({{asset('assets/now-ui-kit-v1.1.0/assets//img/bgg6.jpg')}});\">
            </div>
            <div class=\"container\">
                <div class=\"content-center\">
                    <div class=\"photo-container\" style=\"padding-top: 170px;\">
                        <img src=\"\" alt=\"Thumbnail Image\" class=\"rounded-circle img-fluid img-raised\" style=\"height: 170px; width: 170px;\">
                        <h4 class=\"title\">{{ user.name }}</h4>
                        <p class=\"category\">{{ user.username }}</p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class=\"section\">
            <div class=\"container\">
                {# About Me #}
                <h3 class=\"title\">About me</h3>
                <h5 class=\"description\">{{ user.about }}</h5>
                {# Blog #}
                
                {{ form_start(form) }}
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title text-primary\"><i class=\"fa fa-edit\"></i> Blog</h3>
                    </div>
                    {# title #}
                    <div class=\"col-md-12\">
                      
                        {{ form_widget(form.title) }}

                    </div>
                    {# topic #}
                    <div class=\"col-md-12\">
                        {{ form_widget(form.topic) }}                        
                        <datalist id=\"browsers\">  
                            <option value=\"Self improvement/Self-Hypnosis\">
                            <option value=\"Health & Fitness for Busy People\">
                            <option value=\"Language Learning Blogs\">
                            <option value=\"How to Travel on a Budget (Best hotel deals. Car rental. Trip advice.)\">
                            <option value=\"Writing Style\">
                            <option value=\"Rescued Animals\">
                            <option value=\"Ghost-hunting\">
                            <option value=\"Healthy eating blog\">
                            <option value=\"Beyond the basics of personal financial management\">
                            <option value=\"Hamburgers\">
                            <option value=\"Getting Microsoft Certified\">
                            <option value=\"Careers vs job: following your passion\">
                            <option value=\"How to have a Strong Marriage\">
                            <option value=\"Getting Microsoft Certified\">
                        </datalist>
                    </div>
                    {# blog #}
                    <div class=\"col-md-12\">
                        {{ form_widget(form.blog_mess) }}                        
                    </div>
                    {# attachment #}
                   <i class=\"fa fa-paperclip\"></i> Attachment: 
                        {{ form_widget(form.blog_img) }}                        
                    <div class=\"col-sm-6\"></div>
                        <a href=\"#\" class=\"btn btn-default Default\">CLEAR</a>
                        <input type=\"submit\" class=\"btn btn-primary Primary\" value=\"POST\" />
                </div>   
                {{ form_end(form) }}

    

    
        
   

    {# <ul>
        <li>
            <a href=\"{{ path('post_index') }}\">Back to the list</a>
        </li>
    </ul> #}


                {# POSTED BLOG #}


                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <h3 class=\"title\" style=\"margin-top:20px;\">Posted Blog</h3>
                    </div>
                    {% for r in posts %} 
                    <div class=\"row\">       
                        {# postedblog1 #}
                        
                        {# <div class=\"col-md-3 text-center\">
                            <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets//img/team_gra.jpg')}}\" alt=\"Thumbnail Image\" class=\"rounded-circle img-raised2\" style=\"height: 120px; width: 120px; margin-top:20px;\"><br>
                            <h7 style=\"color: gray;\">Gra Ayun<br>07/07/07 10:00pm</h7>
                        </div>
                        <div class=\"col-md-9\" style=\"padding-left: 0px;\">
                            <textarea disabled name=\"postedblog\" class=\"distxarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\"></textarea>
                        </div>
                        <div class=\"col-md-3\"></div>
                            <button class=\"btn btn-simple btn-primary Primary\" data-toggle=\"modal\" data-target=\"#modal_edit\">
                                <i class=\"fa fa-pencil\" style=\"font-color:#f96332; font-size:15px;\"></i>
                            </button>
                            <button class=\"btn btn-simple btn-primary Primary\"><i class=\"fa fa-trash\" style=\"font-color:#f96332; font-size:15px;\"></i></button>
                        <br> #}
                        {# postedblog2 #}   
                        <div class=\"col-md-12\"></div>
                        <div class=\"col-md-3 text-center\">
                            <img src=\"\" alt=\"Thumbnail Image\" class=\"rounded-circle img-raised2\" style=\"height: 120px; width: 120px; margin-top:20px;\"><br>
                            <h7 style=\"color: gray;\">{{r.username}}<br>{{r.posted_at}}</h7>
                        </div>
                        
                            <div class=\"col-md-9\" style=\"padding-left: 0px;\">
                                {#  name=\"postedblog\" \"></textarea> #}
                                <h6 class=\"title\" style=\"margin-top:20px;\">TITLE: {{r.title}}</h6>
                                <p>TOPIC: {{r.topic}}</p>
                                <textarea disabled class=\"distxarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\">{{r.blog_mess}}</textarea>
                            </div>
                    </div> 
                    {% endfor %}
                </div>
            </div>
        </div>

        <div class=\"modal fade modal-mini modal-primary\" id=\"modal_edit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                    <div class=\"modal-header justify-content-center\">
                        <div class=\"modal-profile\">
                            <i class=\"fa fa-edit\"></i>
                        </div>
                    </div>
                    <div class=\"modal-body\">
                        <div class=\"col-md-12\">
                            <textarea name=\"editblog\" class=\"txarea\" rows=\"8\" cols=\"50\" form=\"userform\" style=\"margin-top:20px;\"></textarea>
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        
                        <button type=\"button\" class=\"btn btn-link btn-neutral\">Save</button>
                        <button type=\"button\" class=\"btn btn-link btn-neutral\" data-dismiss=\"modal\">Discard</button>
                    </div>
                </div>
            </div>
        </div>

{% endblock %}

", "body/body_profile.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_profile.html.twig");
    }
}
