<?php

/* base.html.twig */
class __TwigTemplate_022e5fbe7553c6a016dcf07c24e69cb5d15b6be45fa6378c7d9809f11e8f699d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b30825d7347f6c084817f832467f4c98c199fa11c2198a649ea6ec12e0375d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b30825d7347f6c084817f832467f4c98c199fa11c2198a649ea6ec12e0375d9->enter($__internal_1b30825d7347f6c084817f832467f4c98c199fa11c2198a649ea6ec12e0375d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_441329515ffffe3b91ed6cd697f43a34cfc30788b1768a7cf7db560fce35ee23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_441329515ffffe3b91ed6cd697f43a34cfc30788b1768a7cf7db560fce35ee23->enter($__internal_441329515ffffe3b91ed6cd697f43a34cfc30788b1768a7cf7db560fce35ee23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        $this->loadTemplate("core/header.html.twig", "base.html.twig", 1)->display($context);
        echo "    
";
        // line 2
        $this->loadTemplate("body/body_header.html.twig", "base.html.twig", 2)->display($context);
        echo "    
    ";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        // line 5
        $this->loadTemplate("body/body_footer.html.twig", "base.html.twig", 5)->display($context);
        // line 6
        $this->loadTemplate("core/footer.html.twig", "base.html.twig", 6)->display($context);
        echo "   
     ";
        
        $__internal_1b30825d7347f6c084817f832467f4c98c199fa11c2198a649ea6ec12e0375d9->leave($__internal_1b30825d7347f6c084817f832467f4c98c199fa11c2198a649ea6ec12e0375d9_prof);

        
        $__internal_441329515ffffe3b91ed6cd697f43a34cfc30788b1768a7cf7db560fce35ee23->leave($__internal_441329515ffffe3b91ed6cd697f43a34cfc30788b1768a7cf7db560fce35ee23_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4a6cc457c2c67b68956bc7bcc259ad664c1742c1afd4436d97925ab759ea7173 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a6cc457c2c67b68956bc7bcc259ad664c1742c1afd4436d97925ab759ea7173->enter($__internal_4a6cc457c2c67b68956bc7bcc259ad664c1742c1afd4436d97925ab759ea7173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_91f3419bf8ef5aca3279da2a81c2b8c1c0bdece5dadfc7ca824776ba32e32587 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91f3419bf8ef5aca3279da2a81c2b8c1c0bdece5dadfc7ca824776ba32e32587->enter($__internal_91f3419bf8ef5aca3279da2a81c2b8c1c0bdece5dadfc7ca824776ba32e32587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        
        $__internal_91f3419bf8ef5aca3279da2a81c2b8c1c0bdece5dadfc7ca824776ba32e32587->leave($__internal_91f3419bf8ef5aca3279da2a81c2b8c1c0bdece5dadfc7ca824776ba32e32587_prof);

        
        $__internal_4a6cc457c2c67b68956bc7bcc259ad664c1742c1afd4436d97925ab759ea7173->leave($__internal_4a6cc457c2c67b68956bc7bcc259ad664c1742c1afd4436d97925ab759ea7173_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 4,  50 => 3,  38 => 6,  36 => 5,  34 => 3,  30 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'core/header.html.twig' %}    
{% include 'body/body_header.html.twig' %}    
    {% block body %}
    {% endblock %}
{% include 'body/body_footer.html.twig' %}
{% include 'core/footer.html.twig' %}   
     ", "base.html.twig", "/home/babypandalabs/microblog/app/Resources/views/base.html.twig");
    }
}
