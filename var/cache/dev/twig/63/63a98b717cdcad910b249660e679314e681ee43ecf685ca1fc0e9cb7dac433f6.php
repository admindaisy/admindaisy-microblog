<?php

/* post/show.html.twig */
class __TwigTemplate_8252f68d05dcc4cd06b40af32cddcd1f2484594413c02209b43155be5a8b2c40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f2f316d2552fe3000ccbb4d738bee451d2d9c57d7d0e1bd1a8d58dc19461256 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f2f316d2552fe3000ccbb4d738bee451d2d9c57d7d0e1bd1a8d58dc19461256->enter($__internal_3f2f316d2552fe3000ccbb4d738bee451d2d9c57d7d0e1bd1a8d58dc19461256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/show.html.twig"));

        $__internal_cd1e60f17896fc47995780a07202a4f65d24449793911e3a6db85e9b2f29932c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd1e60f17896fc47995780a07202a4f65d24449793911e3a6db85e9b2f29932c->enter($__internal_cd1e60f17896fc47995780a07202a4f65d24449793911e3a6db85e9b2f29932c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f2f316d2552fe3000ccbb4d738bee451d2d9c57d7d0e1bd1a8d58dc19461256->leave($__internal_3f2f316d2552fe3000ccbb4d738bee451d2d9c57d7d0e1bd1a8d58dc19461256_prof);

        
        $__internal_cd1e60f17896fc47995780a07202a4f65d24449793911e3a6db85e9b2f29932c->leave($__internal_cd1e60f17896fc47995780a07202a4f65d24449793911e3a6db85e9b2f29932c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0d35f90649e1ff463f26e50d042ffe5afd12a52fbc178613c812e096d3ba2aad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d35f90649e1ff463f26e50d042ffe5afd12a52fbc178613c812e096d3ba2aad->enter($__internal_0d35f90649e1ff463f26e50d042ffe5afd12a52fbc178613c812e096d3ba2aad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e065fdcc7a36e0928d9cfa893ad262769a403456872e70dd652f669e600bd01a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e065fdcc7a36e0928d9cfa893ad262769a403456872e70dd652f669e600bd01a->enter($__internal_e065fdcc7a36e0928d9cfa893ad262769a403456872e70dd652f669e600bd01a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Post</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Username</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "username", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Topic</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "topic", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Blog_mess</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "blogmess", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Blog_img</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "blogimg", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Posted_at</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "postedat", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_edit", array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 49
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>

";
        
        $__internal_e065fdcc7a36e0928d9cfa893ad262769a403456872e70dd652f669e600bd01a->leave($__internal_e065fdcc7a36e0928d9cfa893ad262769a403456872e70dd652f669e600bd01a_prof);

        
        $__internal_0d35f90649e1ff463f26e50d042ffe5afd12a52fbc178613c812e096d3ba2aad->leave($__internal_0d35f90649e1ff463f26e50d042ffe5afd12a52fbc178613c812e096d3ba2aad_prof);

    }

    public function getTemplateName()
    {
        return "post/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 49,  121 => 47,  115 => 44,  109 => 41,  99 => 34,  92 => 30,  85 => 26,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Post</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ post.id }}</td>
            </tr>
            <tr>
                <th>Username</th>
                <td>{{ post.username }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ post.title }}</td>
            </tr>
            <tr>
                <th>Topic</th>
                <td>{{ post.topic }}</td>
            </tr>
            <tr>
                <th>Blog_mess</th>
                <td>{{ post.blogmess }}</td>
            </tr>
            <tr>
                <th>Blog_img</th>
                <td>{{ post.blogimg }}</td>
            </tr>
            <tr>
                <th>Posted_at</th>
                <td>{{ post.postedat }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('post_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('post_edit', { 'id': post.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>

{% endblock %}
", "post/show.html.twig", "/home/babypandalabs/microblog/app/Resources/views/post/show.html.twig");
    }
}
