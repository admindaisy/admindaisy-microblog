<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_700e27bf84f30427b5402758d1ce32fc3c94c811e4b40ec89d665fab41cbd0a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_460a973d32eb9985a75efe77089a9746328fe28e0bc801355d57273e09ff9b78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_460a973d32eb9985a75efe77089a9746328fe28e0bc801355d57273e09ff9b78->enter($__internal_460a973d32eb9985a75efe77089a9746328fe28e0bc801355d57273e09ff9b78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_19f206c9545a0d03bfb177337a6737a89a74f4144761376c9a0b2706efbb5379 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19f206c9545a0d03bfb177337a6737a89a74f4144761376c9a0b2706efbb5379->enter($__internal_19f206c9545a0d03bfb177337a6737a89a74f4144761376c9a0b2706efbb5379_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_460a973d32eb9985a75efe77089a9746328fe28e0bc801355d57273e09ff9b78->leave($__internal_460a973d32eb9985a75efe77089a9746328fe28e0bc801355d57273e09ff9b78_prof);

        
        $__internal_19f206c9545a0d03bfb177337a6737a89a74f4144761376c9a0b2706efbb5379->leave($__internal_19f206c9545a0d03bfb177337a6737a89a74f4144761376c9a0b2706efbb5379_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bdb524abf9770ad14f2ae8c5628247b725b4995e66303ad51c0d2ff0bf4b0a38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bdb524abf9770ad14f2ae8c5628247b725b4995e66303ad51c0d2ff0bf4b0a38->enter($__internal_bdb524abf9770ad14f2ae8c5628247b725b4995e66303ad51c0d2ff0bf4b0a38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_791b77b53cd1ca31b246f59984b1adf123c584990cdcb4c0d2a7ce7bddf6cafd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_791b77b53cd1ca31b246f59984b1adf123c584990cdcb4c0d2a7ce7bddf6cafd->enter($__internal_791b77b53cd1ca31b246f59984b1adf123c584990cdcb4c0d2a7ce7bddf6cafd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_791b77b53cd1ca31b246f59984b1adf123c584990cdcb4c0d2a7ce7bddf6cafd->leave($__internal_791b77b53cd1ca31b246f59984b1adf123c584990cdcb4c0d2a7ce7bddf6cafd_prof);

        
        $__internal_bdb524abf9770ad14f2ae8c5628247b725b4995e66303ad51c0d2ff0bf4b0a38->leave($__internal_bdb524abf9770ad14f2ae8c5628247b725b4995e66303ad51c0d2ff0bf4b0a38_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Profile/show.html.twig");
    }
}
