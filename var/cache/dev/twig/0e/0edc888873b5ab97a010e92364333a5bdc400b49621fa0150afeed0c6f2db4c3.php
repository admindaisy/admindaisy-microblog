<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_1af66a7709083f8d10e5cce0cd47cabdde3406347e5f1936f2724ae849423cc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_375372718ea1085442bad4bb33e65f94e16f6268cc3c1e6cb403cceb01830207 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_375372718ea1085442bad4bb33e65f94e16f6268cc3c1e6cb403cceb01830207->enter($__internal_375372718ea1085442bad4bb33e65f94e16f6268cc3c1e6cb403cceb01830207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_480f4043d09dbdb9d0bdfc4e93f887de1aa9ede17246b3863d96704da65f0359 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_480f4043d09dbdb9d0bdfc4e93f887de1aa9ede17246b3863d96704da65f0359->enter($__internal_480f4043d09dbdb9d0bdfc4e93f887de1aa9ede17246b3863d96704da65f0359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 1
        echo "
";
        // line 3
        echo "
";
        // line 4
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 5
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 7
        echo "
    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"> 
        </div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-login card-plain\">
                        <div class=\"header header-primary text-center\">
                            <div class=\"logo-container\">
                                <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/blog.png"), "html", null, true);
        echo "\" alt=\"\">
                            </div>
                        </div>
                        <form action=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                        ";
        // line 20
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token"))) {
            // line 21
            echo "                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
                        ";
        }
        // line 23
        echo "                        <div class=\"content\">
                            ";
        // line 25
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                                ";
        // line 28
        echo "                                <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" class=\"form-control\" placeholder=\"Username\" />
                            </div>
                            ";
        // line 31
        echo "                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                                ";
        // line 34
        echo "                                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"Password\" />
                            </div>
                        </div>
                        <div class=\"footer text-center\">
                            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-round btn-lg btn-block\" />
                        </div>
                        </form>
                        <div class=\"pull-right\">
                            <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\" class=\"link\"><i class=\"fa fa-caret-right\"></i> SIGN UP</a>
                        </div>
                    
                </div>
            </div>
        </div>
    ";
        // line 48
        $this->loadTemplate("body/body_footer.html.twig", "@FOSUser/Security/login_content.html.twig", 48)->display($context);
        // line 49
        echo "
";
        
        $__internal_375372718ea1085442bad4bb33e65f94e16f6268cc3c1e6cb403cceb01830207->leave($__internal_375372718ea1085442bad4bb33e65f94e16f6268cc3c1e6cb403cceb01830207_prof);

        
        $__internal_480f4043d09dbdb9d0bdfc4e93f887de1aa9ede17246b3863d96704da65f0359->leave($__internal_480f4043d09dbdb9d0bdfc4e93f887de1aa9ede17246b3863d96704da65f0359_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 49,  110 => 48,  101 => 42,  94 => 38,  88 => 34,  84 => 31,  78 => 28,  74 => 25,  71 => 23,  65 => 21,  63 => 20,  59 => 19,  53 => 16,  43 => 9,  39 => 7,  33 => 5,  31 => 4,  28 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% trans_default_domain 'FOSUserBundle' %}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url({{asset('assets/now-ui-kit-v1.1.0/assets/img/login.jpg')}})\"> 
        </div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-login card-plain\">
                        <div class=\"header header-primary text-center\">
                            <div class=\"logo-container\">
                                <img src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/img/blog.png')}}\" alt=\"\">
                            </div>
                        </div>
                        <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
                        {% if csrf_token %}
                            <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
                        {% endif %}
                        <div class=\"content\">
                            {# username #}
                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                                {# <label for=\"username\">{{ 'security.login.username'|trans }}</label> #}
                                <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" class=\"form-control\" placeholder=\"Username\" />
                            </div>
                            {# password #}
                            <div class=\"input-group form-group-no-border input-lg\">
                                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                                {# <label for=\"password\">{{ 'security.login.password'|trans }}</label> #}
                                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" placeholder=\"Password\" />
                            </div>
                        </div>
                        <div class=\"footer text-center\">
                            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" class=\"btn btn-primary btn-round btn-lg btn-block\" />
                        </div>
                        </form>
                        <div class=\"pull-right\">
                            <a href=\"{{ path(\"fos_user_registration_register\") }}\" class=\"link\"><i class=\"fa fa-caret-right\"></i> SIGN UP</a>
                        </div>
                    
                </div>
            </div>
        </div>
    {% include 'body/body_footer.html.twig' %}

", "@FOSUser/Security/login_content.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
