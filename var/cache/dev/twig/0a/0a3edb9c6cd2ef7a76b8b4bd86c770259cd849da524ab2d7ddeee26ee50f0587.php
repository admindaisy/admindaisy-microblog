<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_1fb507363248ecd7908daec1fd65bdaa984058148b95c8a998e9f5930c8c4ec3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4338a68eb1ea5e5b84b0015537c72d4e29bc1b2e42589a8ba0e0cf0510dd15f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4338a68eb1ea5e5b84b0015537c72d4e29bc1b2e42589a8ba0e0cf0510dd15f->enter($__internal_b4338a68eb1ea5e5b84b0015537c72d4e29bc1b2e42589a8ba0e0cf0510dd15f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_52f5cacfd5b9a4b8a5014c456e8c48279dc08db68f29480950fb658662bdb736 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52f5cacfd5b9a4b8a5014c456e8c48279dc08db68f29480950fb658662bdb736->enter($__internal_52f5cacfd5b9a4b8a5014c456e8c48279dc08db68f29480950fb658662bdb736_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4338a68eb1ea5e5b84b0015537c72d4e29bc1b2e42589a8ba0e0cf0510dd15f->leave($__internal_b4338a68eb1ea5e5b84b0015537c72d4e29bc1b2e42589a8ba0e0cf0510dd15f_prof);

        
        $__internal_52f5cacfd5b9a4b8a5014c456e8c48279dc08db68f29480950fb658662bdb736->leave($__internal_52f5cacfd5b9a4b8a5014c456e8c48279dc08db68f29480950fb658662bdb736_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5403281194c762a2c5a0d382bba0edde793b8eb3587c020bdd1ee5f72c41ebe5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5403281194c762a2c5a0d382bba0edde793b8eb3587c020bdd1ee5f72c41ebe5->enter($__internal_5403281194c762a2c5a0d382bba0edde793b8eb3587c020bdd1ee5f72c41ebe5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_889b4eccf7f6a26c436e51c2237504c9794eaf92a6160e067322fff8190331ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_889b4eccf7f6a26c436e51c2237504c9794eaf92a6160e067322fff8190331ec->enter($__internal_889b4eccf7f6a26c436e51c2237504c9794eaf92a6160e067322fff8190331ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_889b4eccf7f6a26c436e51c2237504c9794eaf92a6160e067322fff8190331ec->leave($__internal_889b4eccf7f6a26c436e51c2237504c9794eaf92a6160e067322fff8190331ec_prof);

        
        $__internal_5403281194c762a2c5a0d382bba0edde793b8eb3587c020bdd1ee5f72c41ebe5->leave($__internal_5403281194c762a2c5a0d382bba0edde793b8eb3587c020bdd1ee5f72c41ebe5_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "/home/babypandalabs/microblog/app/Resources/FOSUserBundle/views/Registration/confirmed.html.twig");
    }
}
