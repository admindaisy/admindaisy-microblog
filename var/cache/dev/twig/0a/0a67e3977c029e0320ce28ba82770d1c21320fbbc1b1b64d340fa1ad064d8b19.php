<?php

/* core/footer.html.twig */
class __TwigTemplate_4cdfbd6ee10456761b55f6eb10944798829597b8f6025e07b4e332cbc4fba9bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts_lib' => array($this, 'block_javascripts_lib'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7462c66dbc117b6d8c810281faf9f5b49ef9d97fb727b95b58a2d18a40b68bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7462c66dbc117b6d8c810281faf9f5b49ef9d97fb727b95b58a2d18a40b68bb->enter($__internal_d7462c66dbc117b6d8c810281faf9f5b49ef9d97fb727b95b58a2d18a40b68bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "core/footer.html.twig"));

        $__internal_e610f497e94f608688fb8985f00e09496a36100371e5c88ecd1f1f88a032d324 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e610f497e94f608688fb8985f00e09496a36100371e5c88ecd1f1f88a032d324->enter($__internal_e610f497e94f608688fb8985f00e09496a36100371e5c88ecd1f1f88a032d324_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "core/footer.html.twig"));

        // line 1
        echo "    \t";
        $this->displayBlock('javascripts_lib', $context, $blocks);
        // line 24
        echo "       
    </body>
</html>";
        
        $__internal_d7462c66dbc117b6d8c810281faf9f5b49ef9d97fb727b95b58a2d18a40b68bb->leave($__internal_d7462c66dbc117b6d8c810281faf9f5b49ef9d97fb727b95b58a2d18a40b68bb_prof);

        
        $__internal_e610f497e94f608688fb8985f00e09496a36100371e5c88ecd1f1f88a032d324->leave($__internal_e610f497e94f608688fb8985f00e09496a36100371e5c88ecd1f1f88a032d324_prof);

    }

    // line 1
    public function block_javascripts_lib($context, array $blocks = array())
    {
        $__internal_c0fc8257d61f1f795e29fa8b22cc6cb8c026ea13306e4940fc032beae050cc95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0fc8257d61f1f795e29fa8b22cc6cb8c026ea13306e4940fc032beae050cc95->enter($__internal_c0fc8257d61f1f795e29fa8b22cc6cb8c026ea13306e4940fc032beae050cc95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts_lib"));

        $__internal_d73fae891d0be0271eba64b3364bfef63219f10b6828cb05b4a08ac8484bae9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d73fae891d0be0271eba64b3364bfef63219f10b6828cb05b4a08ac8484bae9e->enter($__internal_d73fae891d0be0271eba64b3364bfef63219f10b6828cb05b4a08ac8484bae9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts_lib"));

        // line 2
        echo "        \t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/jquery.3.2.1.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/popper.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/core/bootstrap.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-switch.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/nouislider.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t\t\t<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/js/now-ui-kit.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
    \t\t<script type=\"text/javascript\">
    \t\t\t\$(document).ready(function() {
                \tnowuiKit.initSliders();
              \t});
\t\t\t\tfunction scrollToDownload() {
\t\t\t\tif (\$('.section-download').length != 0) {
            \t\t\$(\"html, body\").animate({
                \t\tscrollTop: \$('.section-download').offset().top
            \t\t}, 1000);
        \t\t}}
\t\t\t</script>

\t\t\t<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.3.2.1.min.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script>
    \t";
        
        $__internal_d73fae891d0be0271eba64b3364bfef63219f10b6828cb05b4a08ac8484bae9e->leave($__internal_d73fae891d0be0271eba64b3364bfef63219f10b6828cb05b4a08ac8484bae9e_prof);

        
        $__internal_c0fc8257d61f1f795e29fa8b22cc6cb8c026ea13306e4940fc032beae050cc95->leave($__internal_c0fc8257d61f1f795e29fa8b22cc6cb8c026ea13306e4940fc032beae050cc95_prof);

    }

    public function getTemplateName()
    {
        return "core/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  99 => 23,  95 => 22,  79 => 9,  75 => 8,  71 => 7,  67 => 6,  63 => 5,  59 => 4,  55 => 3,  50 => 2,  41 => 1,  29 => 24,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    \t{% block javascripts_lib %}
        \t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/core/jquery.3.2.1.min.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/core/popper.min.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/core/bootstrap.min.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-switch.js')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/plugins/nouislider.min.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js')}}\" type=\"text/javascript\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/plugins/bootstrap-datepicker.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/now-ui-kit-v1.1.0/assets/js/now-ui-kit.js\" type=\"text/javascript')}}\"></script>
    \t\t<script type=\"text/javascript\">
    \t\t\t\$(document).ready(function() {
                \tnowuiKit.initSliders();
              \t});
\t\t\t\tfunction scrollToDownload() {
\t\t\t\tif (\$('.section-download').length != 0) {
            \t\t\$(\"html, body\").animate({
                \t\tscrollTop: \$('.section-download').offset().top
            \t\t}, 1000);
        \t\t}}
\t\t\t</script>

\t\t\t<script src=\"{{asset('assets/js/bootstrap.js\" type=\"text/javascript')}}\"></script>
\t\t\t<script src=\"{{asset('assets/js/jquery.3.2.1.min.js\" type=\"text/javascript')}}\"></script>
    \t{% endblock %}       
    </body>
</html>", "core/footer.html.twig", "/home/babypandalabs/microblog/app/Resources/views/core/footer.html.twig");
    }
}
