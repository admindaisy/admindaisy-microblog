<?php

/* body/body_signup.html.twig */
class __TwigTemplate_5ce274aa1cb2a274c8ed145ef5e6e46ac3c8c9b71b75454b732c72b57602c511 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "body/body_signup.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96bff048f66a4a5996349790de1a60f70a5b0f018f83442dbed79e3e2dc0669f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96bff048f66a4a5996349790de1a60f70a5b0f018f83442dbed79e3e2dc0669f->enter($__internal_96bff048f66a4a5996349790de1a60f70a5b0f018f83442dbed79e3e2dc0669f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_signup.html.twig"));

        $__internal_c427cf5a52d10c5076fe9871e9d2018d0ae21c7a4bf909d9df751c1e97cb9235 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c427cf5a52d10c5076fe9871e9d2018d0ae21c7a4bf909d9df751c1e97cb9235->enter($__internal_c427cf5a52d10c5076fe9871e9d2018d0ae21c7a4bf909d9df751c1e97cb9235_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "body/body_signup.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_96bff048f66a4a5996349790de1a60f70a5b0f018f83442dbed79e3e2dc0669f->leave($__internal_96bff048f66a4a5996349790de1a60f70a5b0f018f83442dbed79e3e2dc0669f_prof);

        
        $__internal_c427cf5a52d10c5076fe9871e9d2018d0ae21c7a4bf909d9df751c1e97cb9235->leave($__internal_c427cf5a52d10c5076fe9871e9d2018d0ae21c7a4bf909d9df751c1e97cb9235_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_dc5fd88cf75c4d1c46fae401336c2acd2a170dd900fc8043c8eae53d61ac6a87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc5fd88cf75c4d1c46fae401336c2acd2a170dd900fc8043c8eae53d61ac6a87->enter($__internal_dc5fd88cf75c4d1c46fae401336c2acd2a170dd900fc8043c8eae53d61ac6a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_82f8a8816d8dad07efdce34bb737a41ce9b94e57d3af5a1a595a330e884ba592 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82f8a8816d8dad07efdce34bb737a41ce9b94e57d3af5a1a595a330e884ba592->enter($__internal_82f8a8816d8dad07efdce34bb737a41ce9b94e57d3af5a1a595a330e884ba592_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
<body>
";
        // line 6
        echo "
 
";
        // line 9
        echo "    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url(";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/now-ui-kit-v1.1.0/assets/img/login.jpg"), "html", null, true);
        echo ")\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-signup\" data-background-color=\"orange\">
                   \t<div class=\"header text-center\">
                        <h4 class=\"title title-up\">Sign Up</h4>
                   \t</div>
                    <div class=\"card-body\">
                    \t";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "

                    <div class=\"card-body\">
                        
                            ";
        // line 22
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prof_pic", array()), 'widget');
        echo "
                        
                        <div class=\"footer text-center\">
                            <input type=\"submit\" class=\"btn btn-neutral btn-round btn-lg\" />
                        </div>
                    ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "                     
                \t</div>
            \t</div>
        \t</div>

";
        // line 33
        echo "        \t<footer class=\"footer\">
            \t<div class=\"container\">
                \t<div class=\"copyright\">&copy;
                    \t<script>document.write(new Date().getFullYear())</script>, Designed by
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>. Coded by 
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>.
                \t</div>
           \t\t</div>
        \t</footer>    
    \t</div>
    </div>

";
        
        $__internal_82f8a8816d8dad07efdce34bb737a41ce9b94e57d3af5a1a595a330e884ba592->leave($__internal_82f8a8816d8dad07efdce34bb737a41ce9b94e57d3af5a1a595a330e884ba592_prof);

        
        $__internal_dc5fd88cf75c4d1c46fae401336c2acd2a170dd900fc8043c8eae53d61ac6a87->leave($__internal_dc5fd88cf75c4d1c46fae401336c2acd2a170dd900fc8043c8eae53d61ac6a87_prof);

    }

    public function getTemplateName()
    {
        return "body/body_signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 33,  86 => 27,  78 => 22,  71 => 18,  60 => 10,  57 => 9,  53 => 6,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body%}

<body>
{# Header #}

 
{# Body #}
    <div class=\"page-header\" filter-color=\"orange\">
        <div class=\"page-header-image\" style=\"background-image:url({{asset('assets/now-ui-kit-v1.1.0/assets/img/login.jpg')}})\"></div>
        <div class=\"container\">
            <div class=\"col-md-4 content-center\">
                <div class=\"card card-signup\" data-background-color=\"orange\">
                   \t<div class=\"header text-center\">
                        <h4 class=\"title title-up\">Sign Up</h4>
                   \t</div>
                    <div class=\"card-body\">
                    \t{{ form_start(form) }}

                    <div class=\"card-body\">
                        
                            {{ form_widget(form.prof_pic) }}
                        
                        <div class=\"footer text-center\">
                            <input type=\"submit\" class=\"btn btn-neutral btn-round btn-lg\" />
                        </div>
                    {{ form_end(form) }}                     
                \t</div>
            \t</div>
        \t</div>

{# Footer #}
        \t<footer class=\"footer\">
            \t<div class=\"container\">
                \t<div class=\"copyright\">&copy;
                    \t<script>document.write(new Date().getFullYear())</script>, Designed by
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>. Coded by 
                    \t<a href=\"#\" target=\"_blank\">After Laughter</a>.
                \t</div>
           \t\t</div>
        \t</footer>    
    \t</div>
    </div>

{% endblock %}", "body/body_signup.html.twig", "/home/babypandalabs/microblog/app/Resources/views/body/body_signup.html.twig");
    }
}
